
#include<stdio.h>

int attendance(double);
double revenue(double);
double costs(double);
double profit(double);

int attendance(double price)
{
    return 120-(((price-15)/5)*20);
}
double revenue(double price)
{
    return attendance(price)*price;
}
double costs(double price)
{
    return 500+ (attendance(price)*3);
}
double profit(double price)
{
    return revenue(price)-costs(price);
}
int main()
{
    double price;
    printf("Profit is :\n\n");
    for(price=5;price<=45;price+=5)
    {
        printf("When the ticket price=Rs.%.2f the profit=Rs.%.2f\n",price,profit(price));
        printf("\n");


    }
    return 0;
}
